var player_names = {'tj': 'Lionel Messi',
                    'x9x': 'David Silva',
                    '6b': 'Cristiano Ronaldo',
                    'xme': 'Dani Alves',
                    'azp': 'Fernandinho',
                    'd4v': 'Thiago Silva',
                    '0kc': 'Thomas Müller',
                    'x9e': 'Sergio Agüero',
                    'jox': 'Gareth Bale',
                    '6z': 'Luka Modrić'};

var player_ratings = _.reduce(player_names,
                              function (acc, v, k, xs) {
                                  acc[k] = eval("player_" + k);
                                  return acc;
                              },
                              {});

var player_ages = _.reduce(player_names,
                           function(acc, v, k, xs) {
                               acc[k] = eval("age_" + k);
                               return acc;
                           },
                           {});

var players = [];

var colors = chroma.scale('Set1').colors(_.size(player_names));

function fetch_ratings(player_id) {
    return player_ratings[player_id];
}

function generate_confidence(ratings) {
    // Generate the standard deviation at each sample.
    var raw = Array.from({length: ratings.length}, (v, k) => 5/(Math.pow(k+1, 0.2)));

    // Generate the corresponding values for the upper and lower
    // bounds at each sample.
    var lower = _.map(ratings, function(v, i, _) { return v - raw[i]; });
    var upper = _.map(ratings, function(v, i, _) { return v + raw[i]; });

    return [lower, upper]
}

function generate_minutes(ratings) {
    return _.map(ratings, function(v, i, x) {
        return 100.0 * _.random(true);
    });
}

function parseDate(timestamp) {
    return new Date(timestamp * 1000);
}

function plot(type, player_id, name, color, timestamps, ratings, confidence, minutes) {
    var rev_timestamps = _.reverse(_.clone(timestamps));
    
    confidence_trace = {
        x: _.concat(timestamps, rev_timestamps),
        y: _.concat(confidence[1], _.reverse(confidence[0])),
        xaxis: 'x',
        yaxis: 'y',
        name: name + ' confidence',
        type: 'scatter',
        mode: 'lines',
        hoverinfo: 'none',
        showlegend: false,
        fill: 'toself',
        fillcolor: chroma(color).alpha(0.1).css(),
        line: {
            color: 'transparent'
        }
    };

    ratings_trace = {
        x: timestamps,
        y: ratings,
        xaxis: 'x',
        yaxis: 'y',
        name: name,
        type: 'scatter',
        mode: 'lines',
        line: {
            color: color
        }
    };

    minutes_trace = {
        x: timestamps,
        y: minutes,
        showlegend: false,
        xaxis: 'x' + (_.size(players) + 1),
        yaxis: 'y' + (_.size(players) + 1),
        name: name + ' participation rate',
        type: 'bar',
        marker: {
            color: color,
            opacity: 0.5
        }
    };

    /*
    minutes_trace = {
        x: timestamps,
        y: [player_id],
        z: minutes,
        showlegend: false,
        xaxis: 'x2',
        yaxis: 'y2',
        type: 'heatmap',
        colorscale: [[0, chroma(color).alpha(0.0).css()],
                     [1, color]],
        showscale: false
        };
    */

    var main_plot_pct = 0.8;
    var bar_plot_pct = 1.0 - main_plot_pct;
    var bar_plot_row_pct = bar_plot_pct / 10.0;

    var graph_layout = {
        margin: {
            l: 60,
            r: 40,
            b: 40,
            t: 40,
            pad: 4
        },
        xaxis: {domain: [0.0, 1.0],
                anchor: 'y',
                showticklabels: false,
                showline: true},
        yaxis: {domain: [(1.0 - main_plot_pct), 1.0],
                anchor: 'x',
                hoverformat: '.2f'},
        //xaxis2: {domain: [0.0, 1.0], anchor: 'y2'},
        //yaxis2: {domain: [0.0, bar_plot_pct], anchor: 'x2'}
    };

    // If this is not our first plot, then don't bother defining the
    // full layout.
    var player_num = _.size(players);
    if (player_num > 1) {
        graph_layout = $('#kx_plot')[0].layout;
        graph_layout['xaxis' + (player_num)]['showticklabels'] = false;
    }

    var xaxis_type = 'date';
    if (_.isEqual(type, 'age')) {
        xaxis_type = '-';
    }
    graph_layout['xaxis']['type'] = xaxis_type;
    graph_layout['xaxis' + (player_num + 1)] = {domain: [0.0, 1.0],
                                                anchor: 'y' + (player_num + 1),
                                                matches: 'x',
                                                showline: false,
                                                showticklabels: true};
    graph_layout['yaxis' + (player_num + 1)] = {domain: [(bar_plot_pct - (player_num * bar_plot_row_pct)),
                                                (bar_plot_pct - ((player_num - 1) * bar_plot_row_pct))],
                                                anchor: 'x' + (player_num + 1),
                                                showticklabels: false,
                                                showgrid: false,
                                                zeroline: false,
                                                hoverformat: '.2f%'};
    graph_layout['xaxis' + (player_num + 1)]['type'] = xaxis_type;

    // Add this player to the existing ratings plot.
    Plotly.plot($('#kx_plot')[0], [confidence_trace, ratings_trace, minutes_trace],
                graph_layout,
                {displaylogo: false, displayModeBar: false, responsive: true});

    var bar_layout = {
        showlegend: false,
    };
}

function plot_player(player_id, type) {
    players.push(player_id);
    var player_name = player_names[player_id];

    // Pick a color for this player (we don't care if it's unique for
    // this demo).
    var color = colors[_.size(players) - 1]

    // Fetch player data.
    var ratings = fetch_ratings(player_id);

    // Separate the timestamp and rating values into their own arrays.
    var data = _.unzip(ratings);
    ratings = data[1];

    // By default, just transform unix epoch timestamps into JS
    // timestamps (milliseconds). But if the 'age' button is selected,
    // convert to age in years.
    var transform = function(x) { return x * 1000; };
    if (_.isEqual(type, 'age')) {
        var birthday = player_ages[player_id];
        transform = function(x) { return (x - birthday) / (365.25 * 24 * 60 * 60); };
    }
    var timestamps = _.map(data[0], transform);

    // Generate confidence data.
    var confidence = generate_confidence(data[1]);

    // Generate minutes played.
    var minutes = generate_minutes(data[1]);

    // Plot the player's ratings and confidence interval.
    plot(type, player_id, player_name, color, timestamps, ratings, confidence, minutes);
}

function clear() {
    // Nuke the existing plot.
    Plotly.purge($('#kx_plot')[0]);

    // Empty the players list.
    _.remove(players);
}

function toggle_age_axis(is_age) {
    $('#age_toggle').prop('disabled', is_age);
    $('#time_toggle').prop('disabled', !is_age);
}

$( document ).ready(function() {
    $('#age_toggle').click(function() {
        $('#kx_plot').fadeTo("fast", 0.5, function() {
            $('.loader').toggle(400, function() {
                clear();
                for (var p in player_names) {
                    plot_player(p, 'age');
                }
                toggle_age_axis(true);
                $('.loader').toggle(400, function() {
                    $('#kx_plot').fadeTo("fast", 1.0);
                });
            });
        });
    });
    
    $('#time_toggle').click(function() {
        $('#kx_plot').fadeTo("fast", 0.5, function() {
            $('.loader').toggle(400, function() {
                clear();
                for (var p in player_names) {
                    plot_player(p, 'time');
                }
                toggle_age_axis(false);
                $('.loader').toggle(400, function() {
                    $('#kx_plot').fadeTo("fast", 1.0);
                });
            });
        });
    });

    $('#time_toggle').click();
});


